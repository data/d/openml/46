# OpenML dataset: splice

https://www.openml.org/d/46

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Genbank. Donated by G. Towell, M. Noordewier, and J. Shavlik  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Molecular+Biology+(Splice-junction+Gene+Sequences))   
**Please cite**:  None  

Primate splice-junction gene sequences (DNA) with associated imperfect domain theory.
Splice junctions are points on a DNA sequence at which 'superfluous' DNA is removed during the process of protein creation in higher organisms. The problem posed in this dataset is to recognize, given a sequence of DNA, the boundaries between exons (the parts of the DNA sequence retained after splicing) and introns (the parts of the DNA sequence that are spliced out). This problem consists of two subtasks: recognizing exon/intron boundaries (referred to as EI sites), and recognizing intron/exon boundaries (IE sites). (In the biological community, IE borders are referred to a ''acceptors'' while EI borders are referred to as ''donors''.)

All examples taken from Genbank 64.1. Categories "ei" and "ie" include every "split-gene" for primates in Genbank 64.1. Non-splice examples taken from sequences known not to include a splicing site.
         
### Attribute Information 
>
              1   One of {n ei ie}, indicating the class.
              2   The instance name.
           3-62   The remaining 60 fields are the sequence, starting at 
                  position -30 and ending at position +30. Each of
                  these fields is almost always filled by one of 
                  {a, g, t, c}. Other characters indicate ambiguity among
                  the standard characters according to the following table:
    character: meaning
        D: A or G or T
        N: A or G or C or T
        S: C or G
        R: A or G

Notes:  
* Instance_name is an identifier and should be ignored for modelling

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46) of an [OpenML dataset](https://www.openml.org/d/46). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

